sphinx-ttrpg
============

A Sphinx_ extension for helping nerdy dungeon masters!

- Releases_
- Documentation_
- `Source code`_

.. _Sphinx: https://www.sphinx-doc.org/en/master/
.. _Releases: https://pypi.org/project/sphinx-ttrpg/
.. _Documentation: https://cblegare.gitlab.io/sphinx-ttrpg
.. _Source code: https://gitlab.com/cblegare/sphinx-ttrpg