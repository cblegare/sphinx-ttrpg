import sphinx_ttrpg


def test_setup_return_keys():
    class Sphinx:
        def add_domain(self, domain):
            pass

    app = Sphinx()
    returned_dict = sphinx_ttrpg.setup(app)
    assert "version" in returned_dict
    assert "parallel_read_safe" in returned_dict
    assert "parallel_write_safe" in returned_dict
