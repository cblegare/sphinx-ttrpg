.. _manual:

###########
User Manual
###########

Characters
==========

You can use |sphinx-ttrpg| to define `characters`, friendly or hostile.

.. admonition:: Example

    .. code-block:: rst
        :linenos:

        .. ttrpg:character:: Lord Gardakan
            Gardakan
            The Hospitable

            This lvl 20 paladin is famous for reasons.

    **Which would render like so:**

    .. ttrpg:character:: Lord Gardakan
            Gardakan
            The Hospitable

            This lvl 20 paladin is famous for reasons.

You use the ``:character:`` Sphinx role to link to it.

.. admonition:: Example

    .. code-block:: rst

        During winter, :ttrpg:character:`Gardakan` mostly sleeps.

        Also, :ttrpg:character:`Mordak` is dead.

    **Which would render like so:**

    During winter, :ttrpg:character:`Gardakan` mostly sleeps.

    Also, :ttrpg:character:`Mordak` is dead.

A domain index for characters is produced.  You can link to it
with the ``:ref:`` Sphinx role.

.. admonition:: Example

    .. code-block:: rst

        See also :ref:`ttrpg-characterindex`

    **Which would render like so:**

    See also :ref:`ttrpg-characterindex`

You can of course refer to other characters from within a character
directive:

.. admonition:: Example

    .. code-block:: rst

        .. ttrpg:character:: Mordak

            Was a wizard.  He was killed by :ttrpg:character:`The Hospitable`.

    **Which would render like so:**

    .. ttrpg:character:: Mordak

        Was a wizard.  He was killed by :ttrpg:character:`The Hospitable`.

Within the ``.. ttrpg:character::`` directive, you can still use
any Sphinx compatible Restructured text.

.. admonition:: Example

    .. code-block:: rst

        .. ttrpg:character:: Regular Restructured Text Markup

            Within the ``.. ttrpg:character::`` directive, you can still use
            any Sphinx compatible Restructured text.

            .. admonition:: Even this!

                Yep.

    **Which would render like so:**

    .. ttrpg:character:: Regular Restructured Text Markup

        Within the ``.. ttrpg:character::`` directive, you can still use
        any Sphinx compatible Restructured text.

        .. admonition:: Even this!

            Yep.

These characters are listed in the ``ttrpg`` domain index.
The characters and its efferent links are also added in the :ref:`genindex`.
