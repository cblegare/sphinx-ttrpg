##############
|sphinx-ttrpg|
##############


.. only:: html

    |home_badge| |lic_badge| |pdf_badge|

    .. |lic_badge| image:: https://img.shields.io/badge/License-LGPL%20v3-blue.svg?logo=gnu
        :target: http://www.gnu.org/licenses/lgpl-3.0
        :alt: GNU Lesser General Public License v3, see :ref:`license`.

    .. |home_badge| image:: https://img.shields.io/badge/sources-cblegare%2Fsphinx--ttrpg-orange?logo=git
        :target: https://gitlab.com/cblegare/sphinx-ttrpg
        :alt: Home Page

    .. |doc_badge| image:: https://img.shields.io/badge/docs-sphinx--ttrpg-blue.svg
        :target: https://cblegare.gitlab.io/sphinx-ttrpg
        :alt: Home Page

    .. |pdf_badge| image:: https://img.shields.io/badge/docs-sphinx--ttrpg-red.svg?logo=adobe-acrobat-reader
        :target: |pdflink|
        :alt: Home Page


.. only:: latex

    The latest version of this document is also available online in the Docs_.

    .. _Docs: https://cblegare.gitlab.io/sphinx-ttrpg/


.. only:: html

     Download this documentation as a |pdflink|.


.. toctree::
    :maxdepth: 2
    :glob:

    manual
    modules


======================
Technical Informations
======================

.. only:: html

    |build_badge| |cov_badge|

    .. |build_badge| image:: https://gitlab.com/cblegare/sphinx-ttrpg/badges/master/pipeline.svg
        :target: https://gitlab.com/cblegare/sphinx-ttrpg/pipelines
        :alt: Build Status

    .. |cov_badge| image:: https://gitlab.com/cblegare/sphinx-ttrpg/badges/master/coverage.svg
        :target: _static/embedded/coverage/index.html
        :alt: Coverage Report


.. only:: html

    ==================
    Tables and indices
    ==================

    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`



.. _project's home: https://gitlab.com/cblegare/sphinx-ttrpg/
