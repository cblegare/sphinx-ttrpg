import configparser
import logging
import os
import shutil
import subprocess
import sys
from pathlib import Path
from typing import TYPE_CHECKING, Any, Dict, List, Optional, Union

import click

import sphinx_ttrpg
from tool.error import SubprocessFailed
from tool.junit import combine_junit_reports
from tool.proc import run_cmd
from tool.sphinx import Sphinx

if TYPE_CHECKING:
    CompletedProcess = subprocess.CompletedProcess[Any]
else:
    CompletedProcess = subprocess.CompletedProcess

log = logging.getLogger("tool")

DEFAULT_PYPROJECT_ROOT = Path(__file__).parent.parent.parent
DEFAULT_PYSRC_ROOT = DEFAULT_PYPROJECT_ROOT.joinpath("src")
DEFAULT_PYTEST_ROOT = DEFAULT_PYPROJECT_ROOT.joinpath("test")
TOOLSRC_ROOT = DEFAULT_PYPROJECT_ROOT.joinpath("tool/tool")
DEFAULT_BUILD_ROOT = DEFAULT_PYPROJECT_ROOT.joinpath("build")
DEFAULT_REPORT_ROOT = DEFAULT_BUILD_ROOT.joinpath("report")
DEFAULT_DOC_ROOT = DEFAULT_PYPROJECT_ROOT.joinpath("doc")


class RunConfig(object):
    pyproject_dir: Path = DEFAULT_PYPROJECT_ROOT
    pysrc_dir: Path = DEFAULT_PYSRC_ROOT
    pytest_dir: Path = DEFAULT_PYTEST_ROOT
    doc_dir: Path = DEFAULT_DOC_ROOT
    build_root: Path = DEFAULT_BUILD_ROOT
    report_dir: Path = DEFAULT_REPORT_ROOT


@click.group()
@click.option(
    "-v",
    "--verbose",
    "verbosity",
    count=True,
    default=int(logging.INFO / 10),
    show_default="INFO",
)
@click.pass_context
def main(ctx: click.Context, verbosity: int) -> None:
    """
    Do things
    """
    logging.basicConfig(**logging_configuration(verbosity))
    DEFAULT_REPORT_ROOT.mkdir(parents=True, exist_ok=True)

    ctx.obj = RunConfig()

    log.debug("interpreter : {!s}".format(sys.executable))
    log.debug(
        "project root       : {!s}".format(ctx.obj.pyproject_dir.resolve())
    )
    log.debug("python sources root: {!s}".format(ctx.obj.pysrc_dir.resolve()))
    log.debug("python test root   : {!s}".format(ctx.obj.pysrc_dir.resolve()))
    log.debug("doc dir            : {!s}".format(ctx.obj.doc_dir.resolve()))
    log.debug("build root         : {!s}".format(ctx.obj.build_root.resolve()))
    log.debug("report root        : {!s}".format(ctx.obj.report_dir.resolve()))


@main.command()
@click.pass_obj
def labz(obj: RunConfig) -> None:
    """
    Sandbox!
    """


@main.command()
@click.pass_obj
def rtfd(obj: RunConfig) -> None:
    """
    Open documentation in browser.
    """
    click.launch(str(obj.build_root.joinpath("html/index.html")))


@main.command()
@click.pass_obj
def clean(obj: RunConfig) -> None:
    """
    Clean some files.
    """

    def clean_bytecode(here: Path) -> None:
        log.info("cleaning bytecode at {!s}".format(here))
        for path in here.rglob("*.py[co]"):
            path.unlink()
            log.info("  cleaned {!s}".format(path))

    def clean_pycache(here: Path) -> None:
        log.info("cleaning pycache at {!s}".format(here))
        for path in here.rglob("__pycache__"):
            path.rmdir()
            log.info("  cleaned {!s}".format(path))

    def clean_other_cache(here: Path) -> None:
        log.info("cleaning other cache at {!s}".format(here))
        for path in here.glob("*_cache"):
            shutil.rmtree(str(path))
            log.info("  cleaned {!s}".format(path))

    log.info("cleaning {!s}".format(obj.pyproject_dir))
    clean_bytecode(obj.pyproject_dir)
    clean_pycache(obj.pyproject_dir)
    clean_other_cache(obj.pyproject_dir)


@main.group("format", invoke_without_command=True)
@click.pass_context
def reformat(ctx: click.Context) -> None:
    """
    Format code.

    When no commands are provided, all commands are run.
    """
    if ctx.invoked_subcommand is None:
        run_subcommands(imports, code)


@reformat.command()
@click.pass_obj
def imports(obj: RunConfig) -> None:
    """
    Reorders python imports.
    """
    paths = [
        obj.pysrc_dir,
        DEFAULT_PYPROJECT_ROOT.joinpath("tool"),
        obj.pytest_dir,
    ]

    cmd: List[Union[str, Path]] = ["isort"]
    cmd.extend(paths)

    log.info("Running 'isort' to reorder python imports")
    run_subprocess(cmd, cwd=obj.pyproject_dir)


@reformat.command()
@click.pass_obj
def code(obj: RunConfig) -> None:
    """
    Reformat python code.
    """
    paths = [
        obj.pysrc_dir,
        DEFAULT_PYPROJECT_ROOT.joinpath("tool"),
        obj.pytest_dir,
    ]

    cmd: List[Union[str, Path]] = ["black", "-l80"]
    cmd.extend(paths)

    log.info("Running 'black' to reformat python code")
    run_subprocess(cmd, cwd=obj.pyproject_dir)


@main.group(invoke_without_command=True)
@click.pass_context
def check(ctx: click.Context) -> None:
    """
    Perform checks.

    When no commands are provided, all commands are run.
    """
    if ctx.invoked_subcommand is None:
        run_subcommands(test, style, typing, security, lint, complexity, report)


@check.command()
@click.pass_obj
def test(obj: RunConfig) -> None:
    """
    Run the tests.
    """
    log.info("Running 'pytest' to run tests")
    run_subprocess([sys.executable, "-m", "pytest"], cwd=obj.pyproject_dir)


@check.command()
@click.pass_obj
def style(obj: RunConfig) -> None:
    """
    Check code style.
    """
    paths = [
        obj.pysrc_dir,
        obj.pyproject_dir.joinpath("tool"),
        obj.pytest_dir,
    ]

    cmd: List[Union[str, Path]] = ["black", "-l80", "--check", "--diff"]
    cmd.extend(paths)
    run_subprocess(cmd, cwd=obj.pyproject_dir)


@check.command()
@click.pass_obj
def typing(obj: RunConfig) -> None:
    """
    Check type annotations.
    """
    paths = [
        obj.pysrc_dir,
        obj.pyproject_dir.joinpath("tool"),
    ]

    cmd: List[Union[str, Path]] = ["mypy"]
    cmd.extend(paths)
    run_subprocess(cmd, cwd=obj.pyproject_dir)


@check.command()
@click.pass_obj
def report(obj: RunConfig) -> None:
    """
    Aggregate test coverage reports.
    """
    run_subprocess(["coverage", "report"], cwd=obj.pyproject_dir)
    run_subprocess(["coverage", "html"], cwd=obj.pyproject_dir)
    run_subprocess(["coverage", "xml"], cwd=obj.pyproject_dir)
    combine_junit_reports(
        obj.report_dir.joinpath("junit.xml"),
        *[report for report in obj.report_dir.glob("*junit.xml")],
    )


@check.command()
@click.pass_obj
def security(obj: RunConfig) -> None:
    """
    Check for security vulnerabilities.
    """
    paths = [obj.pysrc_dir]

    cmd: List[Union[str, Path]] = ["bandit", "-r"]
    cmd.extend(paths)
    run_subprocess(cmd, cwd=obj.pyproject_dir)

    cmd = ["bandit", "-f", "json", "-o", obj.report_dir.joinpath("sast.json")]
    cmd.extend(paths)
    run_subprocess(cmd, cwd=obj.pyproject_dir)


@check.command()
@click.pass_obj
def complexity(obj: RunConfig) -> None:
    """
    Check code complexity with Xenon.
    """
    paths = [
        obj.pysrc_dir,
    ]
    cmd: List[Union[str, Path]] = [
        "xenon",
        "--max-average",
        "A",
        "--max-modules",
        "A",
        "--max-absolute",
        "A",
    ]
    cmd.extend(paths)

    log.info("Running 'xenon' to check for complexity")
    run_subprocess(cmd, cwd=obj.pyproject_dir)


@check.command()
@click.pass_obj
def lint(obj: RunConfig) -> None:
    """
    Check for smells in code.
    """
    log.info("Running 'flake8' to check for complexity")
    paths = [
        obj.pysrc_dir,
        obj.pytest_dir,
    ]

    cmd: List[Union[str, Path]] = [
        sys.executable,
        "-m",
        "flake8",
        "--exit-zero",
    ]

    cmd_codeclimate: List[Union[str, Path]] = cmd.copy()
    cmd_codeclimate.extend(
        [
            "--format",
            "gl-codeclimate",
            "--output-file",
            obj.report_dir.joinpath("codeclimate.json"),
        ]
    )
    cmd_codeclimate.extend(paths)
    run_subprocess(cmd_codeclimate, cwd=obj.pyproject_dir)

    cmd_xunit: List[Union[str, Path]] = cmd.copy()
    cmd_xunit.extend(
        [
            "--format",
            "junit-xml",
            "--output-file",
            obj.report_dir.joinpath("flake8_junit.xml"),
        ]
    )
    cmd_xunit.extend(paths)
    run_subprocess(cmd_xunit, cwd=obj.pyproject_dir)


@main.group(invoke_without_command=True)
@click.pass_context
def build(ctx: click.Context) -> None:
    """
    Assemble artifacts.
    """
    if ctx.invoked_subcommand is None:
        run_subcommands(doc, wheel, sdist, requirements)


@build.command()
@click.pass_obj
def wheel(obj: RunConfig) -> None:
    """
    Build binary wheel distribution.
    """
    cmd: List[Union[str, Path]] = [
        sys.executable,
        obj.pyproject_dir.joinpath("setup.py"),
        "bdist_wheel",
    ]
    run_subprocess(cmd, cwd=obj.pyproject_dir)


@build.command()
@click.pass_obj
def requirements(obj: RunConfig) -> None:
    """
    Build a requiements file from dependencies.
    """
    config = configparser.ConfigParser()
    config.read(str(obj.pyproject_dir / "setup.cfg"))
    deps = [
        dep.strip() + os.linesep
        for dep in config["options"]["install_requires"].splitlines()
        if dep
    ]

    with Path(obj.pyproject_dir / "requirements.txt").open("w") as opened:
        opened.writelines(deps)


@build.command()
@click.pass_obj
def sdist(obj: RunConfig) -> None:
    """
    Build source distribution.
    """
    cmd: List[Union[str, Path]] = [
        sys.executable,
        obj.pyproject_dir.joinpath("setup.py"),
        "sdist",
    ]
    run_subprocess(cmd, cwd=obj.pyproject_dir)


@build.command()
@click.pass_obj
def doc(obj: RunConfig) -> None:
    """
    Build PDF and HTML documentation.
    """
    build_root = obj.build_root.joinpath("sphinx")
    artifact_stem = "{!s}-{!s}".format(
        sphinx_ttrpg.__dist__.project_name, sphinx_ttrpg.__dist__.version
    )

    sphinx = Sphinx(obj.doc_dir, build_root)

    sphinx.build_apidoc(obj.pysrc_dir)

    coverage_src = obj.report_dir.joinpath("coverage", "html")
    if coverage_src.exists():
        coverage_dest = obj.doc_dir.joinpath("_static", "embedded", "coverage")
        coverage_dest.parent.mkdir(exist_ok=True, parents=True)
        if coverage_dest.exists():
            shutil.rmtree(coverage_dest)
        shutil.copytree(coverage_src, coverage_dest)

    latex_dir = sphinx.build_doc("latex")
    subprocess.run(["make"], cwd=str(latex_dir))
    obj.doc_dir.joinpath("_static").mkdir(parents=True, exist_ok=True)
    shutil.copy2(
        str(build_root.joinpath("latex", "{!s}.pdf".format(artifact_stem))),
        str(obj.doc_dir.joinpath("_static")),
    )

    sphinx.build_doc("html")
    sphinx.build_doc("linkcheck")


def run_subcommands(*subcommands: click.Command) -> None:
    ctx = click.get_current_context()
    failed = []
    for cmd in subcommands:
        try:
            ctx.invoke(cmd)
        except click.exceptions.Exit as error:
            if error.exit_code:  # type: ignore  # Mypy do not find exit_code
                failed.append(cmd)
        except click.Abort as error:
            failed.append(cmd)

    exit_code = 0

    if failed:
        click.echo("")
        click.echo("---------------", err=True)
        click.echo("Command failed:", err=True)
        for fail in failed:
            click.echo(f"  {fail.name}", err=True)

        exit_code = 1

    raise click.exceptions.Exit(exit_code)


def run_subprocess(
    cmd: List[Union[str, Path]], cwd: Optional[Path] = None
) -> None:
    try:
        run_cmd(cmd, cwd=cwd)
    except SubprocessFailed as e:
        raise click.exceptions.Exit(e.returncode) from e


def logging_configuration(verbosity: int) -> Dict[str, Any]:
    level = logging.CRITICAL - logging.DEBUG * verbosity

    if level < logging.DEBUG:
        format = "%(levelname)8s %(name)-24s> %(message)s"
    else:
        format = "%(levelname)8s: %(message)s"

    return dict(level=level, format=format,)


if __name__ == "__main__":
    main(auto_envvar_prefix="tool", prog_name="tool")
