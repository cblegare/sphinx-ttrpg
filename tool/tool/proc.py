import logging
import subprocess
from pathlib import Path
from typing import TYPE_CHECKING, Any, List, Optional, Union

if TYPE_CHECKING:
    CompletedProcess = subprocess.CompletedProcess[Any]
else:
    CompletedProcess = subprocess.CompletedProcess

log = logging.getLogger("tool")


from tool.error import SubprocessFailed


def run_cmd(
    cmd: List[Union[str, Path]], cwd: Optional[Path] = None
) -> CompletedProcess:
    cmd_as_strings: List[str] = [str(part) for part in cmd]
    cmd_str = " ".join(cmd_as_strings)
    log.debug("Executing '{!s}' in '{!s}'.".format(cmd_str, cwd))
    result = subprocess.run(cmd_as_strings, cwd=cwd)
    if result.returncode:
        raise SubprocessFailed(result.returncode)
    return result
