import logging
from pathlib import Path
from typing import Callable, Dict, List, Optional

log = logging.getLogger("tool")

from tool.error import DependencyMissing


def get_sphinx() -> Callable[[List[str]], int]:
    try:
        import sphinx.cmd.build
    except ImportError as e:
        log.critical("Sphinx could not be imported")
        raise DependencyMissing("sphinx") from e

    return sphinx.cmd.build.main


def get_apidoc() -> Callable[[List[str]], int]:
    try:
        import sphinx.ext.apidoc
    except ImportError as e:
        log.critical("Sphinx could not be imported")
        raise DependencyMissing("sphinx") from e

    return sphinx.ext.apidoc.main


class Sphinx:
    def __init__(
        self,
        doc_root: Path,
        build_root: Path,
        sphinx_run: Optional[Callable[[List[str]], int]] = None,
        apidoc_run: Optional[Callable[[List[str]], int]] = None,
    ):
        self._doc_root = doc_root
        self._build_root = build_root
        self._sphinx = sphinx_run or get_sphinx()
        self._apidoc = apidoc_run or get_apidoc()

    def build_apidoc(
        self,
        src_root: Path,
        module_first: bool = True,
        separate: bool = True,
        maxdepth: int = 4,
        force: bool = False,
    ) -> Path:
        args: List[str] = []
        if module_first:
            args.append("--module-first")
        if separate:
            args.append("--separate")
        if force:
            args.append("--force")
        args.extend(("--maxdepth", str(maxdepth)))
        args.extend(("--output-dir", str(self._doc_root)))

        args.append(str(src_root))

        self._apidoc(args)

        return self._doc_root

    def build_doc(
        self,
        builder: str,
        conf_dir: Optional[Path] = None,
        write_all: bool = False,
        rebuild_env: bool = False,
        tags: Optional[List[str]] = None,
        processes: Optional[int] = None,
        settings: Optional[Dict[str, str]] = None,
        html_values: Optional[Dict[str, str]] = None,
        noconfig: bool = False,
        nitpicky: bool = True,
        nocolor: bool = False,
        quiet: bool = False,
        very_quiet: bool = False,
        issues_to_file: Optional[Path] = None,
        warning_are_errors: bool = False,
        keep_going: bool = False,
        traceback: bool = False,
        run_debugger: bool = False,
    ) -> Path:
        tags = tags or []
        settings = settings or {}
        html_values = html_values or {}

        args: List[str] = [
            "-b",
            builder,
            "-d",
            str(self._build_root.joinpath("doctrees")),
        ]

        if conf_dir:
            args.extend(("-c", str(conf_dir)))
        if write_all:
            args.append("-a")
        if rebuild_env:
            args.append("-E")
        if processes:
            args.extend(("-j", str(processes)))
        if nitpicky:
            args.append("-n")
        if nocolor:
            args.append("-N")
        if noconfig:
            args.append("-C")
        if quiet:
            args.append("-q")
        if very_quiet:
            args.append("-Q")
        if issues_to_file:
            args.extend(("-w", str(issues_to_file)))
        if warning_are_errors:
            args.append("-W")
        if keep_going:
            args.append("--keep-going")
        if traceback:
            args.append("-T")
        if run_debugger:
            args.append("-P")

        for tag in tags:
            args.extend(("-t", tag))
        for key, value in settings.items():
            args.extend(("-D", f"{key}={value}"))
        for key, value in html_values.items():
            args.extend(("-A", f"{key}={value}"))

        destination_dir = self._build_root.joinpath(builder)

        args.append(str(self._doc_root))
        args.append(str(destination_dir))

        self._sphinx(args)

        return destination_dir
