import logging
from pathlib import Path
from typing import Any, Callable, List, Optional

from tool.error import DependencyMissing

log = logging.getLogger("tool")


def get_junit() -> Any:
    try:
        from junitparser import JUnitXml  # type: ignore
    except ImportError as e:
        log.critical("junitparser could not be imported")
        raise DependencyMissing("junitparser") from e

    return JUnitXml


def combine_junit_reports(where: Path, *reports: Path) -> None:
    JUnitXml = get_junit()
    final_report = JUnitXml("report")

    for report in reports:
        log.info(f"appending {report!s}")
        final_report += JUnitXml.fromfile(str(report))

    final_report.write(where, pretty=True)
