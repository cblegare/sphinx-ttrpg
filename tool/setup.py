# type: ignore
from setuptools import find_packages, setup

if __name__ == "__main__":
    setup(
        name="tool",
        version="1.0.0",
        url="https://example.com",
        author="Author Name",
        author_email="author@gmail.com",
        description="Description of my package",
        packages=find_packages(),
        install_requires=[
            "click",
            "pytest",
            "pytest-cov",
            "sphinx",
            "sphinx_rtd_theme",
            "sphinx_paramlinks",
            "black",
            "isort",
            "mypy",
            "bandit",
            "flake8",
            "junitparser",
            "flake8-gl-codeclimate",
            "flake8-formatter-junit-xml",
            "xenon",
        ],
        entry_points={"console_scripts": ["tool=tool.cli:main"],},
    )
