from pathlib import Path
from typing import Any, Dict

from pkg_resources import Distribution, DistributionNotFound, get_distribution
from sphinx.application import Sphinx

_here = Path(__file__).parent
_PACKAGE = "sphinx-ttrpg"
__here__ = str(_here)


try:
    __dist__: Distribution = get_distribution(_PACKAGE)
    __project__ = __dist__.project_name
    __version__ = __dist__.version
except DistributionNotFound:
    from pkg_resources import Distribution

    __dist__ = Distribution(project_name=_PACKAGE, version="(local)")
    __project__ = __dist__.project_name
    __version__ = __dist__.version
except ImportError:
    __project__ = _PACKAGE
    __version__ = "(local)"
else:
    pass


class Error(Exception):
    pass


def setup(app: Sphinx) -> Dict[str, Any]:
    from sphinx_ttrpg.domain import TTRPGDomain

    app.add_domain(TTRPGDomain)

    return {
        "version": "0.1",
        "parallel_read_safe": True,
        "parallel_write_safe": True,
    }
