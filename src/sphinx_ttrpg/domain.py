from collections import defaultdict, namedtuple
from dataclasses import dataclass
from typing import Any, Dict, Iterable, List, Optional, Tuple, cast

from docutils import nodes
from docutils.nodes import Element, Node, system_message
from slugify import (  # type: ignore # see https://gitlab.com/cblegare/sphinx-ttrpg/-/issues/3 # noqa: E501
    slugify,
)
from sphinx import addnodes
from sphinx.addnodes import desc_signature, pending_xref
from sphinx.builders import Builder
from sphinx.directives import ObjectDescription
from sphinx.domains import Domain, Index, IndexEntry
from sphinx.environment import BuildEnvironment
from sphinx.roles import XRefRole
from sphinx.util.nodes import make_refnode

GeneralIndexEntry = namedtuple(
    "GeneralIndexEntry",
    ["entrytype", "entryname", "targetid", "mainname", "key"],
)

DomainObjectDescription = namedtuple(
    "DomainObjectDescription",
    ["name", "dispname", "type", "docname", "anchor", "priority"],
)


@dataclass
class CharacterNameEntry:
    name: str
    target_id: str
    character_id: str


class CharacterDefinition(ObjectDescription):
    """
    Capture the definition of a character within a directive.

    A character can have more than one name, all of which can be used
    for cross referencing.

    Example:

        .. code-block:: rst

            .. ttrpg:character:: Gardakan
                Lord Gardakan
                Gardy

                A mighty paladin!

    Characters defined will

    *   Be available for cross referencing with ``:ttrpg:character:`` role
        (see :class:`sphinx_ttrpg.domain.CharacterRef`)

    *   Be listed in the ``ttrpg`` domain index
        (see :class:`sphinx_ttrpg.domain.CharacterIndex`)

    *   Be listed in the general index.
    """

    has_content = True
    required_arguments = 1
    option_spec: Dict[str, Any] = {}

    def handle_signature(self, sig: str, signode: desc_signature) -> Any:
        """
        Parse the signature into nodes and append them to the signature node.

        More specifically, handle_signature implements parsing the
        signature of the directive and passes on the object’s name
        and type to its superclass.

        .. note:: We don't really care about parsing a character's name,
            hence we return the signature as-is.

        For instance, given the following directive definition

        .. code-block:: rst

            .. ttrpg:character:: Guy McMaskface

                Some description of Guy.

        then ``Guy McMaskface`` will be passed as the `sig` parameter.

        Args:
            sig: The signature, ``Guy McMaskface`` from the the example above.
            signode: The signature document node.  It acts like the `term`
            part of a glossary entry.

        Returns:
            We return a nice a nice unique url friendly anchor name.

            The return value should be a value that identifies the object.
            It is passed to :meth:`add_target_and_index()` unchanged,
            and otherwise only used to skip duplicates.
        """
        identifier = "character-{!s}-{!s}".format(slugify(sig), len(self.names))
        signode += addnodes.desc_name(text=sig)
        return identifier

    def add_target_and_index(
        self, name: Any, sig: str, signode: desc_signature
    ) -> None:
        """
        Add cross-reference IDs and entries to self.indexnode, if applicable.

        This method will add this character name to the general index.

        Args:
            name: The identifying name of this character name.

            sig: The character name we are indexing.

            signode: The signature document node.  It acts like the `term`
                part of a glossary entry.
        """
        signode["ids"].append(name)

        if "noindex" not in self.options:
            # First, register this character to the world building domain
            ttrpg = cast(TTRPGDomain, self.env.get_domain("ttrpg"))
            ttrpg.add_character(
                CharacterNameEntry(
                    name=sig, target_id=name, character_id=self.names[0]
                )
            )
            # Then, add index entries for it
            generalindex_entry = GeneralIndexEntry(
                entrytype="single",
                entryname=sig,
                targetid=name,
                mainname=sig,
                key=None,
            )
            inode = addnodes.index(entries=[generalindex_entry])
            self.indexnode.append(inode)


class CharacterIndex(Index):
    """
    A custom index that creates an NPC matrix.
    """

    name = "characterindex"
    localname = "Character Index"
    shortname = "character"

    def generate(
        self, docnames: Optional[Iterable[str]] = None
    ) -> Tuple[List[Tuple[str, List[IndexEntry]]], bool]:
        """
        Generate character domain index entries.

        Note:
             Entries should be filtered by the docnames provided. To do.

        Args:
            docnames: Restrict source Restructured text documents to these.

        Returns:
            See :meth:`sphinx.domains.Index.generate` for details.
        """
        content_working_copy = defaultdict(list)

        characters: List[DomainObjectDescription] = sorted(
            [
                character_entry[0]
                for character_entry in self.domain.data["characters"].values()
            ],
            key=lambda character_name_entry: character_name_entry.dispname,
        )

        # generate the expected output, shown below, from the above using the
        # first letter of the npc as a key to group thing
        #
        # name, subtype, docname, anchor, extra, qualifier, description
        #
        # This shows:
        #
        #     D
        #   - **Display Name** *(extra info)* **qualifier:** typ
        #       **Sub Entry** *(extra info)* **qualifier:** typ
        SUBTYPE_NORMAL = 0
        SUBTYPE_WITHSUBS = 1  # noqa: F841
        SUBTYPE_SUB = 2  # noqa: F841
        for name, dispname, typ, docname, anchor, _ in characters:
            first_letter = dispname[0].lower()
            content_working_copy[first_letter].append(
                IndexEntry(
                    dispname,
                    SUBTYPE_NORMAL,
                    docname,
                    anchor,
                    "extra info",
                    "qualifier",
                    typ,
                )
            )
        # convert the dict to the sorted list of tuples expected
        content = sorted(content_working_copy.items())

        return content, True


class CharacterRef(XRefRole):
    """
    Define a character reference role.

    These references are added to the general index.
    """

    def result_nodes(
        self,
        document: nodes.document,
        env: "BuildEnvironment",
        node: Element,
        is_ref: bool,
    ) -> Tuple[List[Node], List[system_message]]:
        """
        Add general index nodes just before returning the finished xref nodes.

        Args:
            document: Source document where this ref was defined.
            env: Current Sphinx build environment.
            node: This role's node.
            is_ref: True when this is the reference node, else it's the
                content node.

        Returns:
            Not sure what the second item of this tuple should be.
        """
        entry = ("single", self.target, self.rawtext, "", None)
        inode = addnodes.index(entries=[entry])
        node.append(inode)
        return [node], []


class TTRPGDomain(Domain):
    """
    A Sphinx domain is a specialized container that ties together
    roles, directives, and indices, among other things.
    """

    name = "ttrpg"
    label = "Table Top Roleplaying Game"
    roles = {"character": CharacterRef()}
    directives = {
        "character": CharacterDefinition,
    }
    indices = [
        CharacterIndex,
    ]
    initial_data: Dict[str, Dict[str, List[CharacterNameEntry]]] = {
        "characters": defaultdict(list),  # names indexed by their owner id
    }

    def get_objects(self) -> Iterable[Tuple[str, str, str, str, str, int]]:
        """
        Return an iterable of "object descriptions".

        See Also:
             Parent method :meth:`sphinx.domains.Domain.get_objects`.

        Returns:
            Object descriptions are tuples with six items.
            See :class:`sphinx_ttrpg.domain.DomainObjectDescription`.
        """
        for character_id, character_name_entry in self.data[
            "characters"
        ].items():
            yield from character_name_entry

    def resolve_xref(
        self,
        env: BuildEnvironment,
        fromdocname: str,
        builder: Builder,
        typ: str,
        target: str,
        node: pending_xref,
        contnode: Element,
    ) -> Element:
        """
        Resolve the pending_xref *node* with the given *typ* and *target*.

        Args:
            env: Current Sphinx build environment.
            fromdocname: Document name where xref was used.
            builder: Current Sphinx builder.
            typ: Object type name.
            target: Looked up object identifier.
            node: Document node for the xref.
            contnode: I don't know, really.

        Returns:

        """
        match = [
            (docname, anchor)
            for name, _, _, docname, anchor, _ in self.get_objects()
            if name == target
        ]

        if match:
            docname, anchor = match[0]

            return make_refnode(
                builder, fromdocname, docname, anchor, contnode, anchor
            )
        else:
            raise RuntimeError(
                "Cross reference to '{!s}' "
                "could not be resolved "
                "within the '{!s}' domain".format(target, self.name)
            )

    def add_character(self, character_name: CharacterNameEntry) -> None:
        """
        Add a new character to the domain.

        Args:
            character_name: The new name for the character we are adding.
        """
        obj = DomainObjectDescription(
            name=character_name.name,
            dispname=character_name.name,
            type="character",
            docname=self.env.docname,
            anchor=character_name.target_id,
            priority=0,
        )

        self.data["characters"][character_name.character_id].append(obj)
